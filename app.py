import csv

from flask import Flask, render_template, request, flash

app = Flask(__name__)
app.secret_key = 'my secret key'


@app.route('/')
def index():
    return render_template('index.html')


def write_to_csv(data):
    with open('database.csv', newline='', mode='a') as database:
        name = data["name"]
        email = data["email"]
        subject = data["subject"]
        message = data["message"]
        csv_writer = csv.writer(database, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow([f'name: {name}, email: {email}, subject: {subject}, message: {message}'])


@app.route('/', methods=['POST', 'GET'])
def submit_form():
    if request.method == 'POST':
        try:
            data = request.form.to_dict()
            write_to_csv(data)
            flash('Your message was sent, thank you!')
            return render_template('index.html')
        except:
            flash('Did not saved to database')
            return render_template('index.html')
    else:
        flash('Something went wrong. Try again!')
        return render_template('index.html')


if __name__ == '__main__':
    app.run()
